create table if not exists `SystemVersion`(
    `version_id` INT unsigned auto_increment,
    `version_name` VARCHAR(100) NOT NULL,
    `version_num`   VARCHAR(20) NOT NULL,
    `create_date`   DATE,
    `is_newest`     boolean,
    primary key (`version_id`)
)engine=InnoDB default charset=utf8;


insert into SystemVersion
(version_name,version_num,create_date,is_newest)
values
("XiHu","1.1",NOW(),true);

update SystemVersion set is_newest=false where version_num=1.0;