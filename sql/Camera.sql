create table if not exists `CameraList`(
    `camera_id` INT unsigned auto_increment,
    `camera_name` VARCHAR(100) NOT NULL,
    `camera_num`   VARCHAR(20) NOT NULL,
    `camera_path`   VARCHAR(40) NOT NULL,
    `camera_manufacturer` VARCHAR(20) NOT NULL,
    `create_date`   datetime NOT NULL,
    `is_using`  boolean NOT NULL,
    `is_available`  boolean NOT NULL,
    `camera_width` INT unsigned NOT NULL DEFAULT 1920,
    `camera_height` INT unsigned NOT NULL DEFAULT 1080,
    `camera_frame_rate` INT unsigned NOT NULL DEFAULT 30,
    `camera_bit_rate` INT unsigned NOT NULL DEFAULT 5000000,
    `camera_frame_quality` VARCHAR(20) NOT NULL DEFAULT "high444",
    primary key (`camera_id`)
)engine=InnoDB default charset=utf8;

create table if not exists `CameraUsingList` (
	`using_id` int primary key auto_increment,
    `is_using`  boolean NOT NULL,
    `start_date`   datetime NOT NULL,
    `stop_date`   datetime,
    `camera_num`   VARCHAR(20) NOT NULL,
    `camera_width` INT unsigned NOT NULL,
    `camera_height` INT unsigned NOT NULL,
    `camera_frame_rate` INT unsigned NOT NULL,
    `camera_bit_rate` INT unsigned NOT NULL,
    `camera_frame_quality` VARCHAR(20) NOT NULL
)engine=InnoDB default charset=utf8;

insert into CameraList
(camera_name,camera_num,create_date,is_using,is_available)
values
("晟悦","0",NOW(),false,true);

select * from CameraList;

update CameraList set is_using=false where camera_num=0;