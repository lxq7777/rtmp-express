let mysql = require('mysql')

let connection

function setDBConnection() {
    connection = mysql.createConnection({
        host: 'localhost',
        user: 'admin',
        password: '1234',
        database: 'EdgeDB',
        timezone: 'utc'
    });

    connection.connect(function (err) {
        if (err) {
            return console.error('error: ' + err.message);
        }

        console.log('Connected to the MySQL server.');
    });
}


function getSystemInfo(is_newest) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM SystemVersion WHERE is_newest=${is_newest}`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function maskNewSystemInfo() {
    return new Promise((resolve, reject) => {
        let sql = `update SystemVersion set is_newest=false where is_newest=true;`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function createNewSystemInfo(params) {
    return new Promise(async (resolve, reject) => {
        await maskNewSystemInfo()
        let sql = `insert into SystemVersion (version_name,version_num,create_date,is_newest) values ("${params.version_name}","${params.version_num}",NOW(),true);`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}


function getCameraList(is_available) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM CameraList WHERE is_available=${is_available}`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function getCameraListUsing(camera_num) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM CameraUsingList WHERE camera_num=${camera_num}`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function createNewCamera(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `insert into CameraList (camera_name,camera_num,camera_path,create_date,is_using,is_available,camera_manufacturer) values ("${params.camera_name}","${params.camera_num}","${params.camera_path}",NOW(),false,${params.is_available},"${params.camera_manufacturer}");`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function configCamera(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `update CameraList set camera_width=${params.camera_width},camera_height=${params.camera_height},camera_bit_rate=${params.camera_bit_rate},camera_frame_rate=${params.camera_frame_rate},camera_frame_quality="${params.camera_frame_quality}" where camera_num=${params.camera_num};`;
        console.log(sql);
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function changeCameraAvailable(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `update CameraList set is_available=${params.is_available} where camera_num=${params.camera_num};`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function changeCameraUsing(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `update CameraList set is_using=${params.is_using} where camera_num=${params.camera_num};`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function getCameraInfo(camera_num) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM CameraList WHERE camera_num=${camera_num}`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function getCamerausingDate(params) {
    return new Promise((resolve, reject) => {
        let sql = `SELECT * FROM CameraUsingList WHERE camera_num=${params.camera_num} and using_id=(SELECT MAX(using_id) FROM CameraUsingList WHERE camera_num=${params.camera_num});`;
        connection.query(sql, [true], (error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            resolve(results)
        });
    })
}

function insertNewCameraUsing(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `insert into CameraUsingList (is_using,camera_num,start_date,camera_width,camera_height,camera_frame_rate,camera_bit_rate,camera_frame_quality) values (true,"${params.camera_num}",NOW(),"${params.camera_width}",${params.camera_height},"${params.camera_frame_rate}","${params.camera_bit_rate}","${params.camera_frame_quality}");`;
        connection.query(sql, [true], async(error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            res = await getCamerausingDate(params)
            resolve(res)
        });
    })
}

function updateCameraUsing(params) {
    return new Promise(async (resolve, reject) => {
        let sql = `update CameraUsingList set is_using=false,stop_date=NOW() where camera_num="${params.camera_num}" and is_using=true;`;
        connection.query(sql, [true], async(error, results, fields) => {
            if (error) {
                return console.error(error.message);
            }
            console.log(results);
            res = await getCamerausingDate(params)
            resolve(res)
        });
    })
}

module.exports = { setDBConnection, getSystemInfo, createNewSystemInfo, changeCameraAvailable, changeCameraUsing, getCameraList,getCameraListUsing, createNewCamera, configCamera, getCameraInfo, insertNewCameraUsing, updateCameraUsing }