var express = require('express');
var router = express.Router();
var db = require('../db/connect')
var cp = require("child_process");
var { childProcess } = require('../data/childp')


/* GET camera listing. */
router.get('/listCamera', async function (req, res, next) {
  if (req.query.is_available == null) {
    res.send({ 'status': 'wrong params' })
    return
  }

  console.log(`params is_available -> ${req.query.is_available}`);
  let results = await db.getCameraList(req.query.is_available)
  res.send({ 'status': 'ok', 'info': results })
});

router.get('/listCameraUsing', async function (req, res, next) {
  if (req.query.camera_num == null) {
    res.send({ 'status': 'wrong params' })
    return
  }

  console.log(`params camera_num -> ${req.query.camera_num}`);
  let results = await db.getCameraListUsing(req.query.camera_num)
  res.send({ 'status': 'ok', 'info': results })
});

/* SET new camera. */
router.post('/create_new_camera', async function (req, res, next) {
  if (req.body.camera_name == null || req.body.camera_num == null || req.body.camera_path == null || req.body.is_available == null || req.body.camera_manufacturer == null) {
    res.send({ 'status': 'wrong params' })
    return
  }
  let result = await db.createNewCamera(req.body)
  res.send({ 'status': 'ok', 'info': result })
})

router.post('/config_camera', async function (req, res, next) {
  if (req.body.camera_num == null || req.body.camera_width == null || req.body.camera_height == null || req.body.camera_bit_rate == null || req.body.camera_frame_rate == null || req.body.camera_frame_quality == null) {
    res.send({ 'status': 'wrong params' })
    return
  }
  let result = await db.configCamera(req.body)
  res.send({ 'status': 'ok', 'info': result })
})

router.post('/change_camera_available', async function (req, res, next) {
  if (req.body.is_available == null || req.body.camera_num == null) {
    res.send({ 'status': 'wrong params' })
    return
  }
  let result = await db.changeCameraAvailable(req.body)
  res.send({ 'status': 'ok', 'info': result })
})

router.post('/startCamera', async function (req, res, next) {
  if (req.body.camera_num == null || req.body.rtmp_address == null) {
    res.send({ 'status': 'wrong params' })
    return
  }

  console.log(`params camera_num -> ${req.body.camera_num}`);
  let results = await db.getCameraInfo(req.body.camera_num)

  if (results.length === 0 || results[0].is_available == 0 || results[0].is_using == 1) {
    res.send({ 'status': 'wrong', 'info': 'either no camera or camera is not available or camera is using' })
    return
  }

  child = cp.execFile("/home/pi/Desktop/opencv-rtmp/build/opencv-rtmp", [req.body.rtmp_address, results[0].camera_path, results[0].camera_width, results[0].camera_height, results[0].camera_frame_rate, results[0].camera_bit_rate, results[0].camera_frame_quality], function (err, stdout, stderr) {
    if (err) {
      console.error(err);
    }
    console.log("stdout:", stdout)
    console.log("stderr:", stderr);
  });

  childProcess.push({ 'num': req.body.camera_num, 'childp': child })

  let results2 = await db.changeCameraUsing({ 'camera_num': req.body.camera_num, 'is_using': 1 })

  let results3 = await db.insertNewCameraUsing({ 'camera_num': req.body.camera_num, 'camera_width': results[0].camera_width, 'camera_height': results[0].camera_height, 'camera_frame_rate': results[0].camera_frame_rate, 'camera_bit_rate': results[0].camera_bit_rate, 'camera_frame_quality': results[0].camera_frame_quality })

  res.send({ 'status': 'ok', 'info': [results, results2, results3, "start ok"] })
});

router.post('/stopCamera', async function (req, res, next) {
  if (req.body.camera_num == null) {
    res.send({ 'status': 'wrong params' })
    return
  }

  console.log(`params camera_num -> ${req.body.camera_num}`);
  let results = await db.getCameraInfo(req.body.camera_num)

  if (results.length === 0 || results[0].is_available == 0 || results[0].is_using == 0) {
    res.send({ 'status': 'wrong', 'info': 'either no camera or camera is not available or camera is not using' })
    return
  }

  console.log(`params camera_num -> ${req.body.camera_num}`);
  for (const key in childProcess) {
    if (Object.hasOwnProperty.call(childProcess, key)) {
      const element = childProcess[key];
      if (element.num === req.body.camera_num) {
        element.childp.kill(2)
      }
    }
  }

  let results2 = await db.changeCameraUsing({ 'camera_num': req.body.camera_num, 'is_using': 0 })

  let results3 = await db.updateCameraUsing({ 'camera_num': req.body.camera_num })

  res.send({ 'status': 'ok', 'info': [results, results2, results3, "stop ok"] })
});

module.exports = router;
