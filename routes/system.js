var express = require('express');
var router = express.Router();
var db = require('../db/connect')

/* GET SystemInfo. */
router.get('/', async function (req, res, next) {
  if (req.query.is_newest == null) {
    res.send({ 'status': 'wrong params' })
    return
  }

  console.log(`params is_newest -> ${req.query.is_newest}`);
  let results = await db.getSystemInfo(req.query.is_newest)
  res.send({ 'status': 'ok', 'info': results })
});

/* SET new SystemInfo. */
router.post('/create_new_version', async function (req, res, next) {
  if (req.body.version_name == null || req.body.version_num == null) {
    res.send({ 'status': 'wrong params' })
    return
  }
  let result = await db.createNewSystemInfo(req.body)
  res.send({ 'status': 'ok', 'info': result })
})

module.exports = router;
